package com.hninthiriwai.sqlstudy.repository;

import com.hninthiriwai.sqlstudy.entity.Book;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface BookRepository extends PagingAndSortingRepository<Book, Long> {
}
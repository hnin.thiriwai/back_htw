package com.hninthiriwai.sqlstudy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SqlStudyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SqlStudyApplication.class, args);
	}

}
